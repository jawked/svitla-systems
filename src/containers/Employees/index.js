import React, { useRef, useMemo, useCallback } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Dropdown from "react-dropdown";
import countBy from 'lodash/countBy';

import {
  Table,
  Input,
  Layout,
} from "@components";

/* ACTIONS */
import {
  changeEmployeeStatus,
  applyEmployeesListFilter,
} from '@services/employees/actions';

/* SELECTORS */
import {
  getFilteredEmployeesList,
} from '@services/employees/selectors';

/* CONSTANTS */
import {
  EMPLOYEE_STATUSES,
} from "@services/employees/constants";

const STATUSES = Object.values(EMPLOYEE_STATUSES);
const HEADERS = [
  { id: 'name',         title: 'Name' },
  { id: 'role',         title: 'Role' },
  { id: 'connectedOn',  title: 'Connected On' },
  { id: 'status',       title: 'Status' },
];

const Employees = () => {
  const dispatch = useDispatch();
  const searchRef = useRef(null);
  const employees = useSelector(getFilteredEmployeesList);

  const applyFilter = useCallback(
    e => dispatch(applyEmployeesListFilter(e.target.value)),
    [dispatch],
  );

  const resetFilter = useCallback(
    () => {
      searchRef.current.value = '';
      dispatch(applyEmployeesListFilter(''))
    },
    [dispatch],
  );

  const changeStatus = useCallback(
    (id, status) => dispatch(changeEmployeeStatus(id, status)),
    [dispatch],
  );

  return (
    <Layout.Centered>
      <Layout.Row>
        <Input
          ref={searchRef}
          onChange={applyFilter}
        />
        <button onClick={resetFilter}>
          Reset
        </button>
      </Layout.Row>

      <Layout.Row>
        <Table headers={HEADERS}>
          {useMemo(
            () => employees.map(({ id, status, ...rest }) => ({
              ...rest,
              id,
              status: () =>
                <Dropdown
                  key={id}
                  value={status}
                  options={STATUSES}
                  onChange={({ value }) => changeStatus(id, value)}
                />
            })),
            [employees, changeStatus],
          )}
         </Table>
      </Layout.Row>

      <Layout.Row>
        {useMemo(
          () => STATUSES.map(status =>
            <div>
              <b>{status}: </b>
              {countBy(employees, { status }).true || 0}
            </div>
          ),
          [employees],
        )}
      </Layout.Row>
    </Layout.Centered>
  )
};

export default Employees;
