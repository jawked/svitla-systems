import React from 'react';
import { Provider } from 'react-redux'

import store from '@services/store';
import { Employees } from '@containers';

export default () => (
  <Provider store={store}>
    <Employees />
  </Provider>
);
