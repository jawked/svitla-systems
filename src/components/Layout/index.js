import styled from 'styled-components';

const Row = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const Centered = styled.div`
  display: flex;
  margin: 5% 20%;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export default {
  Row,
  Centered,
};
