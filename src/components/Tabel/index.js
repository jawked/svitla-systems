import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";

import isFunction from 'lodash/isFunction';

const TableRoot =  styled.table`
  width: 100%;
  margin: 25px 0;
  table-layout: fixed;
 
  th {
    text-align: left;  
  }
`;

const Table = ({ headers, children }) => {
  const ths = useMemo(
    () => headers.map(({ id, title }) =>
      <th key={id}>{title}</th>
    ),
    [headers],
  );

  const trs = useMemo(
    () => children.reduce((acc, child) =>
      ([
        ...acc,
        <tr key={child.id}>
          {headers.map(({ id }) =>
            <td key={`${child.id}:${id}`}>
              {isFunction(child[id]) ? child[id]() : child[id] }
            </td>
          )}
        </tr>
      ]),
      []
    ),
    [headers, children],
  );

  return (
    <TableRoot>
      <thead>
        <tr>
          {ths}
        </tr>
      </thead>
      <tbody>
        {trs}
      </tbody>
    </TableRoot>
  )
};

Table.propTypes = {
  headers:  PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    }),
  ),
  children: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      records: PropTypes.arrayOf(
        PropTypes.oneOfType([
          PropTypes.node,
          PropTypes.func,
        ]).isRequired,
      )
    })
  )
};

Table.defaultProps = {
  headers: [],
  children: [],
};

export default Table;
