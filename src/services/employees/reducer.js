import faker from 'faker';
import times from 'lodash/times';
import ih from 'immutability-helper';

import * as types from './types';
import { EMPLOYEE_ROLES, EMPLOYEE_STATUSES } from "./constants";

const INITIAL_STATE = {
  list: [],
  filter: '',
};

{ // MOCKS BLOCK
  const ROLES = Object.values(EMPLOYEE_ROLES);
  const STATUSES = Object.values(EMPLOYEE_STATUSES);
  INITIAL_STATE.list = times(10, id => ({
    id,
    name: faker.name.findName(),
    role: faker.helpers.randomize(ROLES),
    status: faker.helpers.randomize(STATUSES),
    connectedOn: faker.date.past().toDateString(),
  }));
}

const employeesReducer = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case types.EMPLOYEE_CHANGE_STATUS:
      return ih(state, {
        list: {
          $apply: l => l.map(i =>
            i.id !== action.payload.id ? i : ih(i, {
              status: { $set: action.payload.status }
            })
          )
        }
      });
    case types.EMPLOYEES_LIST_FILTER:
      return ih(state, {
        filter: {
          $set: action.payload.filter
        },
      });
    default:
      return state;
  }
};

export default employeesReducer;
