import escapeRegExp from 'lodash/escapeRegExp';

const OR_AND_REGEXP = /\b\s*(or|and)\s*?\b/g;
const createRegExpFromString = (filterStr = '') => (
  new RegExp(
    filterStr.split(OR_AND_REGEXP).reduce(
      (acc, key) => acc.concat(
        { and: '.*', or: '|' }[key] || `(?=.*${escapeRegExp(key.trim())})`
      ),
      ''
    ),
    'gi'
  )
);

const SEARCHABLE_PROPERTIES = ['name', 'connectedOn', 'role', 'status'];
export const getFilteredEmployeesList = ({ employees }) => {
  const matching = createRegExpFromString(employees.filter);

  return employees.list.filter(item =>
    matching.test(
      SEARCHABLE_PROPERTIES
        .map(prop => item[prop])
        .join('')
    )
  );
};
