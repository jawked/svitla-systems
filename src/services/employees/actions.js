import * as types from './types';

export const changeEmployeeStatus = (id, status) => ({
  type: types.EMPLOYEE_CHANGE_STATUS,
  payload: {
    id,
    status,
  }
});

export const applyEmployeesListFilter = filter => ({
  type: types.EMPLOYEES_LIST_FILTER,
  payload: {
    filter,
  }
});
