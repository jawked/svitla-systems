import { createStore, combineReducers } from 'redux';
import employeesReducer from "./employees/reducer";

export default createStore(combineReducers({
  employees: employeesReducer,
}));
