const path = require('path');
const { paths } = require('react-app-rewired');

const resolveAppSrc = target => path.resolve(
  __dirname,
  `${paths.appSrc}/${target}/`
);

module.exports = function override(config) {
  config.resolve.alias = {
    ...config.resolve.alias,
    '@services': resolveAppSrc('services'),
    '@components': resolveAppSrc('components'),
    '@containers': resolveAppSrc('containers'),
  };

  return config;
};
